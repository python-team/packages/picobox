Source: picobox
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Julian Gilbey <jdg@debian.org>
Section: python
Priority: optional
Standards-Version: 4.6.2
Homepage: https://github.com/ikalnytskyi/picobox
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               furo <!nodoc>,
               pybuild-plugin-pyproject,
               python3-all,
               python3-doc <!nodoc>,
               python3-flask <!nocheck>,
               python3-hatch-vcs,
               python3-hatchling,
               python3-pytest <!nocheck>,
               python3-sphinx,
               python3-sphinx-copybutton <!nodoc>
Vcs-Git: https://salsa.debian.org/python-team/packages/picobox.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/picobox
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no
Description: Opinionated Python dependency injection framework
 Picobox is designed to be clean, pragmatic and with Python in mind.
 No complex graphs, no implicit injections, no type bindings - just
 picoboxes and explicit demands.
 .
 It is a small, thread-safe, pure Python library with no dependencies.

Package: python3-picobox
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-picobox-doc
Description: ${source:Synopsis} (Python 3)
 ${source:Extended-Description}
 .
 This package provides the Python library.

Package: python-picobox-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Suggests: python3-picobox
Description: ${source:Synopsis} (documentation)
 ${source:Extended-Description}
 .
 This package contains HTML documentation.
Build-Profiles: <!nodoc>
